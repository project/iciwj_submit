<?php
/**
 * @file
 * Module to generate the XML file ready to upload in the ICI database.
 *
 * Date: 18 lug 2018 15:24:04
 * File: iciwj_submit.admin.inc
 * Author: stefano.
 */

/**
 * Implements drupal_get_form()
 *
 * SC 19 lug 2018 18:16:24 stefano.
 */
function _iciwj_submit_admin_settings($form, &$form_state) {
  // XML.
  //
  // SC 19 lug 2018 18:17:05 stefano.
  //
  $form['iciwj_submit_xml'] = array(
    '#type' => 'fieldset',
    '#title' => t('XML Tags'),
    '#description' => t('The XML static values.'),
    '#weight' => 1,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['iciwj_submit_xml']['iciwj_submit_files'] = array(
    '#type' => 'fieldset',
    '#title' => t('Files'),
    '#weight' => 2,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $file_xsd = drupal_realpath(ICIWJ_SUBMIT_XSD_FILE);

  $form['iciwj_submit_xml']['iciwj_submit_files']['iciwj_submit_xsd_file'] = array(
    '#type' => 'item',
    '#title' => t('XML schema file'),
    '#description' => t('The XSD file to validate the XML. Don\'t remove or rename that file.'),
    '#markup' => $file_xsd ? $file_xsd : t('XML schema file (@filexsd) is missing in the filesystem.', array('@filexsd' => ICIWJ_SUBMIT_XSD_FILE)),
    '#weight' => 2,
    '#required' => TRUE,
  );

  $form['iciwj_submit_xml']['iciwj_submit_journal'] = array(
    '#type' => 'fieldset',
    '#title' => t('Citation information about the journal issue'),
    '#weight' => 3,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
//   $form['iciwj_submit_xml']['iciwj_submit_journal']['iciwj_submit_publishername'] = array(
//     '#type' => 'textfield',
//     '#size' => 50,
//     '#maxlength' => 256,
//     '#title' => t('The publisher name'),
//     '#description' => t('The publisher name.'),
//     '#default_value' => variable_get('iciwj_submit_publishername', ''),
//     '#weight' => 4,
//     '#required' => TRUE,
//   );
//   $form['iciwj_submit_xml']['iciwj_submit_journal']['iciwj_submit_journaltitle'] = array(
//     '#type' => 'textfield',
//     '#size' => 50,
//     '#maxlength' => 256,
//     '#title' => t('Journal title'),
//     '#description' => t('The ICI Title Abbreviation for the journal. If you do not know the abbreviation, see the ICI Catalog.'),
//     '#default_value' => variable_get('iciwj_submit_journaltitle', ''),
//     '#weight' => 5,
//     '#required' => TRUE,
//   );
  $form['iciwj_submit_xml']['iciwj_submit_journal']['iciwj_submit_issn'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('ISSN'),
    '#description' => t('The ISSN of the journal.'),
    '#default_value' => variable_get('iciwj_submit_issn', ''),
    '#weight' => 6,
    '#required' => TRUE,
  );

//   $form['iciwj_submit_xml']['iciwj_submit_copyrightinformation'] = array(
//     '#type' => 'textarea',
//     '#title' => t('Copyright'),
//     '#description' => t('The copyright information associated with the article.'),
//     '#default_value' => variable_get('iciwj_submit_copyrightinformation', ''),
//     '#weight' => 7,
//     '#required' => FALSE,
//   );

//   $form['iciwj_submit_xml']['iciwj_submit_coistatement'] = array(
//     '#type' => 'textarea',
//     '#title' => t('Coi statement'),
//     '#description' => t('The Conflict of Interest statement associated with this article.'),
//     '#default_value' => variable_get('iciwj_submit_coistatement', ''),
//     '#weight' => 8,
//     '#required' => FALSE,
//   );

  // FTP.
  //
  // SC 19 lug 2018 18:17:43 stefano.
  //
//   $form['iciwj_submit_ftp'] = array(
//     '#type' => 'fieldset',
//     '#title' => t('FTP account'),
//     '#description' => t('Contact publisher@ncbi.nlm.nih.gov to request FTP login information.'),
//     '#weight' => 2,
//     '#collapsible' => TRUE,
//     '#collapsed' => TRUE,
//   );

//   $form['iciwj_submit_ftp']['iciwj_submit_username'] = array(
//     '#type' => 'textfield',
//     '#size' => 50,
//     '#maxlength' => 256,
//     '#title' => t('Username'),
//     '#description' => t('Your login name.'),
//     '#default_value' => variable_get('iciwj_submit_username', ''),
//     '#required' => TRUE,
//   );

//   $form['iciwj_submit_ftp']['iciwj_submit_password'] = array(
//     '#type' => 'textfield',
//     '#size' => 50,
//     '#maxlength' => 256,
//     '#title' => t('Password'),
//     '#description' => t('Your password.'),
//     '#default_value' => variable_get('iciwj_submit_password', ''),
//     '#required' => TRUE,
//   );

//   $form['iciwj_submit_ftp']['iciwj_submit_url'] = array(
//     '#type' => 'textfield',
//     '#size' => 50,
//     '#maxlength' => 256,
//     '#title' => t('URL'),
//     '#description' => t('The ftp URL. The URL is absolute (beginning with a scheme such as "ftp:").'),
//     '#default_value' => variable_get('iciwj_submit_url', ''),
//     '#required' => TRUE,
//   );

  // Node.
  //
  // SC 19 lug 2018 18:18:07 stefano.
  //
  $form['iciwj_submit_node'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node'),
    '#description' => t('Options related to node.'),
    '#weight' => 3,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['iciwj_submit_node']['iciwj_submit_ntype'] = array(
    '#type' => 'select',
    '#title' => t('Content type'),
    '#options' => node_type_get_names(),
    '#description' => t('The manuscript content type ready for ICI World of Journal.'),
    '#default_value' => variable_get('iciwj_submit_ntype', array()),
    '#multiple' => TRUE,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 99,
  );

  return $form;
}

function _iciwj_submit_admin_settings_submit($form, &$form_state) {
  // XML.
  //
  // SC 19 lug 2018 18:18:32 stefano.
  //
//   variable_set('iciwj_submit_xsd_file', $form_state['values']['iciwj_submit_xsd_file']);
//   variable_set('iciwj_submit_publishername', $form_state['values']['iciwj_submit_publishername']);
//   variable_set('iciwj_submit_journaltitle', $form_state['values']['iciwj_submit_journaltitle']);
  variable_set('iciwj_submit_issn', $form_state['values']['iciwj_submit_issn']);
//   variable_set('iciwj_submit_copyrightinformation', $form_state['values']['iciwj_submit_copyrightinformation']);
//   variable_set('iciwj_submit_coistatement', $form_state['values']['iciwj_submit_coistatement']);

  // FTP.
  //
  // SC 19 lug 2018 18:18:48 stefano.
  //
//   variable_set('iciwj_submit_username', $form_state['values']['iciwj_submit_username']);
//   variable_set('iciwj_submit_password', $form_state['values']['iciwj_submit_password']);
//   variable_set('iciwj_submit_url', $form_state['values']['iciwj_submit_url']);

  // Node.
  //
  // SC 19 lug 2018 18:18:58 stefano.
  //
  variable_set('iciwj_submit_ntype', $form_state['values']['iciwj_submit_ntype']);

}

function _iciwj_submit_admin_settings_validate($form, &$form_state) {

  $file_xsd = drupal_realpath(ICIWJ_SUBMIT_XSD_FILE);

  if (!$file_xsd || !(file_exists($file_xsd) && is_readable($file_xsd) && filesize($file_xsd))) {
    form_set_error('iciwj_submit_xsd_file', t('XML schema file (@filexsd) is missing in the filesystem.', array('@filexsd' => ICIWJ_SUBMIT_XSD_FILE)));
  }

//   if(!preg_match("|^<!DOCTYPE.+>$|", $form_state['values']['iciwj_submit_xsd_file'])) {
//     form_set_error('iciwj_submit_xsd_file', t('Please, insert the full document type string.'));
//   }
}



