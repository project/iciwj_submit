ICI World of Journals submit

DESCRIPTION
-----------

The ICI World of Journals is one of the largest international databases of 
scientific journals from all over the world, where all periodicals may be 
indexed. Currently, over 43 thousand journals are registered in the database. 
Due to the high interest of journals from all over the world in our database, 
they have introduced improvements and functions aiming at detecting ‘predatory 
journals’ practices. The idea behind the ICI World of Journals database is 
creating a place where scientific journals would undergo such verification.

This module generates, from a node/fields, the XML file ready to upload in 
the ICI database.

REQUIREMENTS
------------

None from Drupal.

The ICI World of Journals requires an own account. 

INSTALLATION
------------

 1. CREATE DIRECTORY

    Create a new directory "iciwj_submit" in the sites/all/modules directory or 
    everywhere you install your modules and place the entire contents of this 
    module in it.

 2. ENABLE THE MODULE

    Enable the module on the Modules admin page.

 3. ACCESS PERMISSION

    Grant the access at the Access control page:
      People > Permissions.

 4. CONFIGURE THE MODULE
 
    

CONFIGURATION
-------------

 1. Go to admin/config/services/iciwj_submit and fill the fields:

    XML TAGS
      Under "Citation information about the journal issue" insert the ISSN of 
      the journal.
    
    NODE
      Select one or more of the manuscript/node content type ready for ICI 
      World of Journal. 
      
      We recomend to create a new content type. A lot of fields are needed to 
      add for ICI World of Journals.
    
 2. API
 
    The module uses a set of API (hooks) to retrieve the data for the XML. 
    Implements your hook functions (only some are required).

