<?php

/**
 * @file
 * Module to generate the XML file ready to upload in the ICI database.
 *
 * Date: 18 lug 2018 15:24:53
 * File: iciwj_submit.api.php
 * Author: stefano.
 */

/**
 * Issue tag.
 *
 * The container for metadata that defines a single issue of a journal. If you
 * want to import several issues, use several 'issue' containers.
 *
 * SC 24 ago 2018 14:41:12 stefano.
 *
 * @param object $node
 *   The node object.
 *
 * @return array
 *   An array of issue attribute.
 *   QName                      Type        Use         Annotation
 *   number                     xs:string   required    The issue number
 *   volume                     xs:string   required    The volume number
 *   year                       xs:integer  required    Publication year
 *   coverDate                  xs:string   optional    Date from the cover
 *   coverUrl                   xs:anyURI   optional    URL to the image with the cover of the issue. Acceptable image formats are JPEG and PNG. The maximum width of the image is 200px.
 *   electronicContentPdfUrl    xs:anyURI   optional    URL to a file with an electronic version of the edition. Permitted formats are PDF. The file will NOT BE shared publicly.
 *   numberOfArticles           xs:integer  optional    Number of scientific articles in this issue.
 *   publicationDate            timestamp   optional    Publication date
 */
function hook_iciwj_submit_issue($node) {
  return array(
    'issue' => array(
      'coverDate' => '',
      'coverUrl' => '',
      'electronicContentPdfUrl' => '',
      'number' => '',
      'numberOfArticles' => 1,
      'publicationDate' => '',
      'volume' => '',
      'year' => '',
    ),
  );
}

/**
 * Article tag.
 *
 * The container for all information about a single journal article. A journal
 * article is required to have at least one language version, one author and
 * article type.
 *
 * SC 27 ago 2018 15:54:08 stefano.
 *
 * @param object $node
 *   The node object.
 *
 * @return array
 *   An array of article attribute.
 *   QName        Type          Use          Annotation
 *   externalId    xs:string      optional    A publisher identifier that can be used to uniquely identify the publication being registered. This identifier is a publisher-assigned number that uniquely identifies the publication.
 *   gicid        xs:string      optional    Publication's GICID identifier.
 *   icid          xs:integer    optional    Publication's ICID identifier.
 */
function hook_iciwj_submit_article($node) {
  return array(
    'article' => array(
      'externalId' => '',
      'gicid' => '',
      'icid' => '',
    ),
  );
}

/**
 * Type tag.
 *
 * Publication type. Please use only values from the list.
 *
 * SC 28 ago 2018 12:24:22 stefano.
 *
 * @param object $node
 *   The node object.
 *
 * @return array
 *   An array of type value.
 *   One of the follow
 *   Enumeration    ORIGINAL_ARTICLE
 *   Enumeration    REVIEW_ARTICLE
 *   Enumeration    SHORT_COMMUNICATION
 *   Enumeration    COMMENTARY_ON_THE_LAW
 *   Enumeration    SCIENTIFIC_REVIEW
 *   Enumeration    GUIDELINES
 *   Enumeration    POPULAR_SCIENCE_ARTICLE
 *   Enumeration    OTHERS_NONCITABLE
 *   Enumeration    OTHERS_CITABLE
 *   Enumeration    CASE_STUDY
 */
function hook_iciwj_submit_type($node) {
  return array('type' => array('ORIGINAL_ARTICLE'));
}

/**
 * Language version tag.
 *
 * The container for all information about a single language version of the
 * publication. Each publication must contain at least one language version.
 * The language versions of the publication, in the ICI World of Journals
 * system, will be presented as separate publications.
 *
 * SC 29 ago 2018 11:03:07 stefano.
 *
 * @param object $node
 *   The node object.
 * @param array $node_i18n
 *   All nodes in the translation set.
 *
 * @return array
 *   An multidimensional array of article attribute and data.
 *
 *   Attribute
 *   QName      Type      Use       Annotation
 *   externalId xs:string optional  A publisher identifier that can be used to uniquely identify the language version of the publication being registered. This identifier is a publisher-assigned number that uniquely identifies the language version of the publication.
 *   language   xs:string required  Publication language code according to ISO 639. Default value: PL
 *
 *   Data
 *     title {1,1} - Title of the publication.
 *     abstract {1,1} - Abstract of the publication.
 *     pdfFileUrl {0,1} - URL to a file with an electronic version of the publication. Permitted formats are PDF. The file will WILL BE shared publicly.
 *     publicationDate {0,1} - Publication date. [timestamp]
 *     pageFrom {0,1} - First page of the publication.
 *     pageTo {0,1} - Last page of the publication.
 *     doi {0,1} - DOI number of the publication.
 *     keywords {0,1} - A container for keywords related to the language version of the publication. Must contain at least one keyword.
 */
function hook_iciwj_submit_article_i18n($node, $node_i18n) {
  return array(
    'languageVersion' => array(
      array(
        'externalId' => '',
        'language' => '',
        'title' => '',
        'abstract' => '',
        'pdfFileUrl' => '',
        'publicationDate' => 0,
        'pageFrom' => '',
        'pageTo' => '',
        'doi' => '',
        'keywords' => array(),
      ),
    ),
  );
}

/**
 * Authors tag.
 *
 * The container for all information about the contributors/authors of the
 * publication. Must contain at least one contributor/author.
 *
 * SC 30 ago 2018 11:10:56 stefano.
 *
 * @param object $node
 *   The node object.
 *
 * @return array
 *   An multidimensional array of authors data.
 *     name {1,1} - First name of the contributor/author. May be submitted as either a full first name or initials. Do not include titles such as "Prof.", "Dr." or other.
 *     name2 {0,1} - Second name of the contributor/author.
 *     surname {1,1} - Surname of the contributor/author.
 *     email {0,1} - Email of the contributor/author.
 *     polishAffiliation {0,1} - Does the institution to which the author affiliates is Polish. [boolean]
 *     order {1,1} - Author's sequence/order number. [integer]
 *     instituteAffiliation {0,1} - The institution with which a contributor/author is affiliated. This element may hold the name and location of an affiliation with which a contributor/author is affiliated.
 *     departmentAffiliation {0,1} -
 *     subjectAffiliation {0,1} -
 *     role {0,1} - Role of the contributor/author. Please use only values from the list.
 *                  Enumeration    LEAD_AUTHOR    Lead author
 *                  Enumeration    AUTHOR    Author (including co-author)
 *                  Enumeration    RESEARCH_PROJECT    Research project
 *                  Enumeration    RESEARCH_EXECUTION    Research execution
 *                  Enumeration    STATISTICAL_ANALYSIS    Statistical analysis
 *                  Enumeration    DATA_INTERPRETATION    Data interpretation
 *                  Enumeration    MANUSCRIPT_PREPARATION    Manuscript preparation
 *                  Enumeration    LITERATURE_OVERVIEW    Literature overview
 *                  Enumeration    RESEARCH_FUNDING    Research Funding
 *     ORCID {0,1} - The ORCID for an contributor/author. Acceptable format: https://orcid.org/0000-0000-0000-0000
 */
function hook_iciwj_submit_authors($node) {
  return array(
    'authors' => array(array(
      'name' => '',
      'name2' => '',
      'surname' => '',
      'email' => '',
      'polishAffiliation' => '',
      'order' => 0,
      'instituteAffiliation' => '',
      'departmentAffiliation' => '',
      'subjectAffiliation' => '',
      'role' => '',
      'ORCID' => '',
    ),
    ),
  );
}

/**
 * References tag.
 *
 * The container for all references/citations from the publication.
 *
 * SC 30 ago 2018 17:18:50 stefano.
 *
 * @param object $node
 *   The node object.
 *
 * @return array
 *   An multidimensional array of references/citations data.
 *     unparsedContent {1,1} - Reference/citation content string.
 *     order {1,1} - Reference/citation sequence number. [integer]
 *     doi {0,1} - Reference/citation DOI number.
 */
function hook_iciwj_submit_references($node) {
  return array(
    'references' => array(array(
      'unparsedContent' => '',
      'order' => 0,
      'doi' => '',
    ),
    ),
  );
}
